const chai = require('chai');

const expect = chai.expect;

const http = require('chai-http');
chai.use(http);

describe('API Test Suite', () => {
    it('Check if post /currency is running', () => {
        chai.request('http://localhost:3001')
            .post('/currency')
            .type('json')
            .send({
                alias: "Shakugan No Shana",
                name: "Kenneth",
                ex: {
                    peso: 0.47,
                    usd: 0.0092,
                    won: 10.93,
                    yuan: 0.065
                }
            })
            .end((err, res) => {
                expect(res).to.not.equal(undefined);
            })
    })
    it('Check if post /currency returns status 400 if name is missing', (done) => {
        chai.request('http://localhost:3001')
            .post('/currency')
            .type('json')
            .send({
                alias: "Shakugan No Shana",
                ex: {
                    peso: 0.47,
                    usd: 0.0092,
                    won: 10.93,
                    yuan: 0.065
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Check if post /currency returns status 400 if name is not a string', (done) => {
        chai.request('http://localhost:3001')
            .post('/currency')
            .type('json')
            .send({
                alias: "Shakugan No Shana",
                name: 45,
                ex: {
                    peso: 0.47,
                    usd: 0.0092,
                    won: 10.93,
                    yuan: 0.065
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Check if post /currency returns status 400 if name is empty', (done) => {
        chai.request('http://localhost:3001')
            .post('/currency')
            .type('json')
            .send({
                alias: "Shakugan No Shana",
                name: null,
                ex: {
                    peso: 0.47,
                    usd: 0.0092,
                    won: 10.93,
                    yuan: 0.065
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done()
            })
    })
    it('Check if post /currency returns status 400 if ex is missing', (done) => {
        chai.request('http://localhost:3001')
            .post('/currency')
            .type('json')
            .send({
                alias: "Shakugan No Shana",
                name: 'Cameron Kenneth'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Check if post /currency returns status 400 if ex is not an object', (done) => {
        chai.request('http://localhost:3001')
            .post('/currency')
            .type('json')
            .send({
                alias: "Shakugan No Shana",
                name: 'Cameron Kenneth',
                ex: 42
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Check if post /currency returns status 400 if ex is empty', (done) => {
        chai.request('http://localhost:3001')
            .post('/currency')
            .type('json')
            .send({
                alias: "Shakugan No Shana",
                name: 'Cameron Kenneth',
                ex: ""
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })       
        it('Check if post /currency returns status 400 if alias is missing', (done) => {
            chai.request('http://localhost:3001')
                .post('/currency')
                .type('json')
                .send({
                    name: 'Cameron Kenneth',
                    ex: {
                        peso: 0.47,
                        usd: 0.0092,
                        won: 10.93,
                        yuan: 0.065
                    }
                })
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                    done();
                })
        })
        it('Check if post /currency returns status 400 if alias is not a string', (done) => {
            chai.request('http://localhost:3001')
                .post('/currency')
                .type('json')
                .send({
                    alias: 32,
                    name: 'Valentine',
                    ex: {
                        peso: 0.47,
                        usd: 0.0092,
                        won: 10.93,
                        yuan: 0.065
                    }
                })
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                    done();
                })
        })
        it('Check if post /currency returns status 400 if alias is empty', (done) => {
            chai.request('http://localhost:3001')
                .post('/currency')
                .type('json')
                .send({
                    alias: null,
                    name: 'Valentine',
                    ex: {
                        peso: 0.47,
                        usd: 0.0092,
                        won: 10.93,
                        yuan: 0.065
                    }
                })
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                    done()
                })
        })
        it('Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias', (done) => {
            chai.request('http://localhost:3001')
                .post('/currency')
                .type('json')
                .send({
                    alias: 'Shanalala',
                    alias: 'Cameron',
                    name: 'Valentine',
                    ex: {
                        peso: 0.47,
                        usd: 0.0092,
                        won: 10.93,
                        yuan: 0.065
                    }
                })
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                    done()
                })
        })
        it('Check if post /currency returns status 200 if all fields are complete and there are no dupicates', (done) => {
            chai.request('http://localhost:3001')
                .post('/currency')
                .type('json')
                .send({
                    alias: 'Shanalala',
                    name: 'Valentine',
                    ex: {
                        peso: 0.47,
                        usd: 0.0092,
                        won: 10.93,
                        yuan: 0.065
                    }
                })
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    done();
                })
        })
    })
