const { currency } = require('../src/util.js');

module.exports = (app) => {

    app.post('/currency', (req, res) => {
        if (!req.body.hasOwnProperty('name')) {
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }
        if (typeof req.body.name !== 'string') {
            return res.status(400).send({
                'error': 'Bad Request - Name has to be a string'
            })
        }
        if (typeof req.body.name === null) {
            return res.status(400).send({
                'error': 'Bad Request - Name is required! '
            })
        }
        if (!req.body.hasOwnProperty('ex')) {
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter EX'
            })
        }
        if (typeof req.body.ex !== 'object') {
            return res.status(400).send({
                'error': 'Bad Request - Ex has to be an object'
            })
        }
        if (typeof req.body.ex === null) {
            return res.status(400).send({
                'error': 'Bad Request - EX is required! '
            })
        }
        if (!req.body.hasOwnProperty('alias')) {
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter ALIAS'
            })
        }
        if (typeof req.body.alias !== 'string') {
            return res.status(400).send({
                'error': 'Bad Request - Alias has to be a string'
            })
        }
        if (typeof req.body.alias === null) {
            return res.status(400).send({
                'error': 'Bad Request - ALIAS is required! '
            })
        }
        if (req.body.length !== req.body.length && req.body.alias !== 1) {
            return res.status(400).send({
                'error': 'Bad Request - Duplicate Alias has been found!'
            })
        }
        if (req.body.length === req.body.length) {
            return res.status(200).send()
        }
    })
}
